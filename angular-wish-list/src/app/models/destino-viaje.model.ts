import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	selected: boolean;
	public servicios: string[];
	id = uuid();
	constructor (public nombre:string,public imagenUrl:string, public votes: number = 0) {
		this.servicios = ['pileta', 'desayuno'];
		this.selected = false;
	 }

	 isSelected(): boolean {
		return this.selected;
	}

	 setSelected(s: boolean) {
		//Object.assign(this.selected, s);
		this.selected = s;
	}

	voteUp(){
		this.votes++;
	}

	voteDown() {
		this.votes--;
	}
}
