import { Injectable } from '@angular/core';
import { Action, State, StateObservable } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { HttpClientModule } from '@angular/common/http';
import { TrackearClickDirective } from '../trackear-click.directive';

//ESTADO
export interface DestinosViajesState{
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function intializeDestinosViajesState(){
    return {
	    items: [],
	    loading: false,
	    favorito: null
    }
}

//ACCIONES
export enum DestinosViajesActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  ELIMINAR = '[Destinos Viajes] Eliminar',
  VOTEUP = '[Destinos Viajes] VoteUp',
  VOTEDOWN = '[Destinos Viajes] VoteDown',
  INIT_MY_DATA = '[Destinos Viajes] Init My Data',
  ADD_TRACK = '[Destinos Viajes] Add Tracks'
}

export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje) {}
}

export class EliminarDestinoAction implements Action{
	type = DestinosViajesActionTypes.ELIMINAR;
	constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action{
	type = DestinosViajesActionTypes.VOTEUP;
	constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action{
	type = DestinosViajesActionTypes.VOTEDOWN;
	constructor(public destino: DestinoViaje) {}
}

export class InitMyDataAction implements Action{
	type = DestinosViajesActionTypes.INIT_MY_DATA;
	constructor(public destinos: string[]) {}
}

 export class AddTrackAction implements Action{
	type = DestinosViajesActionTypes.ADD_TRACK;
	constructor(public tracks: TrackearClickDirective) {}
} 

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | EliminarDestinoAction |
VoteDownAction | VoteUpAction | InitMyDataAction | AddTrackAction ;

//REDUCERS
export function reducerDestinosViajes(
	state:DestinosViajesState,
	action:DestinosViajesActions
) : DestinosViajesState {
	switch (action.type) {
		case DestinosViajesActionTypes.NUEVO_DESTINO: {
		  return {
		  		...state,
		  		items: [...state.items, (action as NuevoDestinoAction).destino ]
		  	};
		}
		case DestinosViajesActionTypes.ELIMINAR: {
			let index =state.items.indexOf((action as EliminarDestinoAction).destino);
					state.items.splice(index,1);
			console.log("eLIMINAr");
			return {
				
				...state,
				items: [...state.items ]
			}
		};
		case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
		    state.items.forEach(x => x.setSelected(false));
            let fav:DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
		    return {
		    	...state,
		  		favorito: fav
		    };
		};
		case DestinosViajesActionTypes.VOTEUP: {
			const d: DestinoViaje = (action as VoteUpAction).destino;
			d.voteUp();
			return { ...state }
		};
		case DestinosViajesActionTypes.VOTEDOWN: {
			const d: DestinoViaje = (action as VoteDownAction).destino;
			d.voteDown();
			return { ...state }
		}
		case DestinosViajesActionTypes.INIT_MY_DATA: {
			const destinos: string[] = (action as InitMyDataAction).destinos;
			return {
				...state,
				items: destinos.map((d) => new DestinoViaje(d,''))
			};
		}
		case DestinosViajesActionTypes.ADD_TRACK: {
			const obj: TrackearClickDirective = (action as AddTrackAction).tracks;
			obj.addTracks();
			return {
				...state
			};
		}
	}
	return state;
}

//EFFECTS
@Injectable()
export class DestinosViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
  	map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions) {}
} 