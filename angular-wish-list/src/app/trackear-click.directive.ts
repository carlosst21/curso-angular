import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Directive, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { fromEvent } from 'rxjs';
import { AppState } from './app.module';
import { AddTrackAction } from './models/destinos-viajes-state.model';
@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLInputElement;
  static traks = 0;

  constructor(private elRef: ElementRef, private store: Store<AppState>) {

    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento));
   }

   log = () => console.log(`Cuenta Tracks #${TrackearClickDirective.traks} `);

   addTracks(){
    TrackearClickDirective.traks++;
   }

    getTracks(): number {
      return TrackearClickDirective.traks;
    }

   track(evento: Event): void {
     const elemTags = this.element.attributes.getNamedItem('data-trackear-tags').value.split(' ');
     console.log(`||||||| track evento: "${elemTags}" `);
     this.store.dispatch(new AddTrackAction(this));
     this.log();
   }

}
