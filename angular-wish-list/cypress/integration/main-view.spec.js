describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-wish-list');
        cy.get('h1 b').should('contain', 'HOLA (es)');
        cy.get('h5').should('contain','Actividad');
        cy.contains('Servicios');
        cy.get('div ').should('contain','Imagen Url');
    });
});
